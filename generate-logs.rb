jarfile = "MINERful.jar"
class_file = "minerful.MinerFulTracesMakerStarter"
alphabet = "A:B:C:D:E:F:G:H:I:J:K:L:M:N:O:P:Q:R:S:T:U:V:W:X:Y:Z"
regexp = "\"[^C]*([AB][C][^C]*)*[^C]*\" \"[^A]*([A][^A]*[BC][^A]*)*[^A]*\" \"[^A]*(([A].*[BCDE].*)|([BCDE].*[A].*))*[^A]*\" \"[^A]*([A].*[BC])*[^A]*\" \"[^E]*([ABCD].*[E])*[^E]*\""
# Fix length and alphabet
sizes = [100,200,500,700,1000,2000,5000,7000]
sizes.each do |size|
  command = "java -cp #{jarfile} #{class_file} -a #{alphabet[0..38]} -d none -m 15 -M 15 -s #{size} -oE \"mxml\" -oLF alpha_20_len_15_size_#{size}.mxml -r #{regexp}"
  system(command)
end

# Fix alphabet and size
lengths = [5,10,15,20,25,30]
lengths.each do |length|
  command="java -cp #{jarfile} #{class_file} -a #{alphabet[0..38]} -d none -m #{length} -M #{length} -s 1000 -oE \"mxml\" -oLF alpha_20_len_#{length}_size_1000.mxml -r #{regexp}"
  system(command)
end

# Fix length and size
slices=[8,18,28,38,48]
slices.each do |slice|
  command="java -cp #{jarfile} #{class_file} -a #{alphabet[0..slice]} -d none -m 15 -M 15 -s 1000 -oE \"mxml\" -oLF alpha_#{(slice + 2) / 2}_len_15_size_1000.mxml -r #{regexp}"
  system(command)
end

puts "DONE!"
