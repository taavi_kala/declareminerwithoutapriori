This is the standalone DeclareMiner application without apriori algorithm.

Usage:

jar -cp declareminerextract_v5.jar BetterMiner path_to_config_dot_properties_file

example conf.properties file with all possible parameters can be found in the root directory