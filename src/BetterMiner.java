import java.io.File;
import java.util.Map;

import org.processmining.plugins.declareminer.DeclareMiner;
import org.processmining.plugins.declareminer.DeclareMinerNoHierarc;
import org.processmining.plugins.declareminer.DeclareMinerNoRed;
import org.processmining.plugins.declareminer.DeclareMinerNoTrans;
import org.processmining.plugins.declareminer.util.Configuration;
import org.processmining.plugins.declareminer.util.DeclareModel;
import org.processmining.plugins.declareminer.visualizing.AssignmentViewBroker;
import org.processmining.plugins.declareminer.visualizing.DGraph;
import org.processmining.plugins.declareminer.visualizing.DeclareMap;
import org.processmining.plugins.declareminer.visualizing.DeclareMinerOutput;
import org.processmining.plugins.declareminer.visualizing.XMLBrokerFactory;

import com.jgraph.layout.JGraphFacade;
import com.jgraph.layout.organic.JGraphOrganicLayout;

public class BetterMiner {	
	public static void main (String args[])  {
		//String configuration_file_path = args[0];
		String configuration_file_path = "config.properties";
		Configuration conf = new Configuration(configuration_file_path);
		
		String miner_type = conf.miner_type;
		String output_file_type = conf.output_file_type;
		String output_path = conf.output_path;
		DeclareMinerOutput output = new DeclareMinerOutput();
		// Choose the Miner type based on configuration
		// TODO
		if (conf.log != null && conf.input != null){
			if (miner_type.equals("DeclareMiner"))
				output = DeclareMiner.mineDeclareConstraints(null, conf.log, conf.input);
			else if (miner_type.equals("DeclareMinerNoHierarc"))
				output = DeclareMinerNoHierarc.mineDeclareConstraints(null, conf.log, conf.input);
			else if (miner_type.equals("DeclareMinerNoRed"))
				output = DeclareMinerNoRed.mineDeclareConstraints(null, conf.log, conf.input);
			else if (miner_type.equals("DeclareMinerNoTrans"))
				output = DeclareMinerNoTrans.mineDeclareConstraints(null, conf.log, conf.input);
			else
				throw new IllegalArgumentException(String.format("Invalid miner type '%s'", miner_type));
				
		} else {
			throw new IllegalArgumentException("No valid argument combination found");
		}
		
		DeclareMap model = output.getModel();
		DeclareModel declare_model = output.getDeclareModel();
		
		File file = new File(output_path);
		String abs_path = file.getAbsolutePath();
		
		if (output_file_type.equals("XML")) {
			// Write declare designer compatible XML
			addLayout(model);
			AssignmentViewBroker broker = XMLBrokerFactory.newAssignmentBroker(abs_path);
			broker.addAssignmentAndView(model.getModel(), model.getView());
		} else if (output_file_type.equals("TEXT")) {
			// Write constraints with supports to txt file
			declare_model.writeConstraintsToFileWithSupport(file);
		} else if (output_file_type.equals("REPORT")) {
			// Write constraints as a human readable file
			declare_model.writeConstraintsAsHumanReadable(file, conf.log.size());
		}	else if (output_file_type.equals("NONE")) {
				
			//	declare_model.writeConstraintsAsHumanReadable(file, conf.log.size());
		} else {
			throw new IllegalArgumentException(String.format("Invalid output file type '%s'"));
		}
		
		System.out.println(abs_path);
		System.out.println("DONE!");
	}
	
	public static void addLayout(DeclareMap map) {
      DGraph graph = map.getView().getGraph();
      final JGraphOrganicLayout oc = new JGraphOrganicLayout();

      oc.setDeterministic(true);
      oc.setOptimizeBorderLine(true);
      oc.setOptimizeEdgeCrossing(true);
      oc.setOptimizeEdgeDistance(true);
      oc.setOptimizeEdgeLength(true);
      oc.setOptimizeNodeDistribution(true);
      oc.setEdgeCrossingCostFactor(999999999);
      oc.setEdgeDistanceCostFactor(999999999);
      oc.setFineTuning(true);
      oc.setEdgeLengthCostFactor(9999);
      if(map.getModel().getConstraintDefinitions().size()<200){
             oc.setEdgeLengthCostFactor(99);
      }
      oc.setNodeDistributionCostFactor(999999999);
      oc.setBorderLineCostFactor(999);
      oc.setRadiusScaleFactor(0.9);
      final JGraphFacade jgf = new JGraphFacade(graph);
      oc.run(jgf);
      @SuppressWarnings("rawtypes")
      final Map nestedMap = jgf.createNestedMap(true, true);
      graph.getGraphLayoutCache().edit(nestedMap);
	}
	
}
