package org.processmining.plugins.declareminer.util;

import java.io.FileWriter;
import java.io.IOException;

public class UnifiedLogger {
	public static String unified_log_path;
	public static String input_log_name;
	public static String pruner_type;
	public static String templates;
	public static int alpha;
	public static int min_support;
	
	public static void log(String time) throws IOException {
		FileWriter fw = new FileWriter(unified_log_path, true);
		String log_line = templates + ";" + alpha + ";" + min_support + ";" + pruner_type + ";" + input_log_name + ";" + String.valueOf(time) + "\n";
		fw.write(log_line);
		fw.close();
	}
}
