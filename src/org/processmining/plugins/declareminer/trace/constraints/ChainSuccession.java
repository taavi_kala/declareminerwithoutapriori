package org.processmining.plugins.declareminer.trace.constraints;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.processmining.plugins.declareminer.enumtypes.DeclareTemplate;
import org.processmining.plugins.declareminer.trace.TemplateReplayer;
import org.processmining.plugins.declareminer.util.DeclareModel;

public class ChainSuccession implements TemplateReplayer {
	private HashMap<String,Integer> seenEvents = null;
	private String lastActivity = null;
	//private Counting<Boolean> finishedTraces = new Counting<Boolean>();
	HashMap<String, HashMap<String, Integer>> fulfilledForThisTracePrecedencePart = new HashMap<String, HashMap<String, Integer>>();
	HashMap<String, HashMap<String, Integer>> fulfilledForThisTraceResponsePart = new HashMap<String, HashMap<String, Integer>>();
	// let's generate precedences
	HashMap<String, HashMap<String, Integer>> satisfiedTraces = new HashMap<String, HashMap<String, Integer>>();
	//HashMap<String, HashMap<String, Integer>> vacuouslySatisfiedTraces = new HashMap<String, HashMap<String, Integer>>();
	HashMap<String, HashMap<String, Integer>> violatedTraces = new HashMap<String, HashMap<String, Integer>>();
	//int satisfiedTraces = 0;
	//int vacuouslySatisfiedTraces = 0;
	//int violatedTraces = 0;
	public ChainSuccession (Map<DeclareTemplate, List<List<String>>> declareTemplateCandidateDispositionsMap){
		for(List<String> params : declareTemplateCandidateDispositionsMap.get(DeclareTemplate.Chain_Succession)){
			String param1 = params.get(0);
			String param2 = params.get(1);
			HashMap<String, Integer> ful = fulfilledForThisTraceResponsePart.get(param1);
			if(ful==null){
				ful = new HashMap<String, Integer>();
			}
			ful.put(param2, 0);
			fulfilledForThisTraceResponsePart.put(param1, ful);

			HashMap<String, Integer> sat = satisfiedTraces.get(param1);
			if(sat==null){
				sat = new HashMap<String, Integer>();
			}
			sat.put(param2, 0);
			satisfiedTraces.put(param1, sat);



			HashMap<String, Integer> viol = violatedTraces.get(param1);
			if(viol==null){
				viol = new HashMap<String, Integer>();
			}
			viol.put(param2, 0);
			violatedTraces.put(param1, viol);
		}

		for(List<String> params : declareTemplateCandidateDispositionsMap.get(DeclareTemplate.Chain_Succession)){
			String param1 = params.get(0);
			String param2 = params.get(1);
			HashMap<String, Integer> ful = fulfilledForThisTracePrecedencePart.get(param1);
			if(ful==null){
				ful = new HashMap<String, Integer>();
			}
			ful.put(param2, 0);
			fulfilledForThisTracePrecedencePart.put(param1, ful);

		}
		seenEvents = new HashMap<String,Integer>();
	}


	@Override
	public void process(String event, boolean isTraceStart, boolean isLastEvent) {
		if(isTraceStart){
			for(String param1 : fulfilledForThisTraceResponsePart.keySet()){
				for(String param2 : fulfilledForThisTraceResponsePart.get(param1).keySet()){
					HashMap<String, Integer> ful = fulfilledForThisTraceResponsePart.get(param1);
					if(ful==null){
						ful = new HashMap<String, Integer>();
					}
					ful.put(param2, 0);
					fulfilledForThisTraceResponsePart.put(param1, ful);
				}
			}

			for(String param1 : fulfilledForThisTracePrecedencePart.keySet()){
				for(String param2 : fulfilledForThisTracePrecedencePart.get(param1).keySet()){
					HashMap<String, Integer> ful = fulfilledForThisTracePrecedencePart.get(param1);
					if(ful==null){
						ful = new HashMap<String, Integer>();
					}
					ful.put(param2, 0);
					fulfilledForThisTracePrecedencePart.put(param1, ful);
				}
			}
			seenEvents = new HashMap<String,Integer>();
			lastActivity = null;
			//pastEvents = new HashMap<String,Integer>();
		}
		//String previous = lastActivity.getItem(caseId);
		if(lastActivity!=null && !lastActivity.equals("") && !lastActivity.equals(event)){
			HashMap<String, Integer> secondElement = new  HashMap<String, Integer>();
			if(fulfilledForThisTraceResponsePart.containsKey(lastActivity)){
				secondElement = fulfilledForThisTraceResponsePart.get(lastActivity);
			}
			int nofull = 0;
			if(secondElement.containsKey(event)){
				nofull = secondElement.get(event);
				secondElement.put(event, nofull+1);
				fulfilledForThisTraceResponsePart.put(lastActivity,secondElement);
			}
		
			//	fulfilledConstraintsPerTraceCh.putItem(caseId, fulfilledForThisTrace);
		}


		//		//update the counter for the current trace and the current event
		//		//**********************
		//
		//		int numberOfEvents = 1;
		//		if(!counter.containsKey(event)){
		//			counter.put(event, numberOfEvents);
		//		}else{
		//			numberOfEvents = counter.get(event);
		//			numberOfEvents++;
		//			counter.put(event, numberOfEvents); 
		//		}
		//		activityLabelsCounterChResponse.putItem(caseId, counter);
		//		lastActivityResponse.putItem(caseId, event);
		//		//***********************
		//		
		//		



		if(lastActivity!=null && !lastActivity.equals("") && !lastActivity.equals(event)){
			HashMap<String, Integer> secondElement = new  HashMap<String, Integer>();
			if(fulfilledForThisTracePrecedencePart.containsKey(lastActivity)){
				secondElement = fulfilledForThisTracePrecedencePart.get(lastActivity);
			}
			int nofull = 0;
			if(secondElement.containsKey(event)){
				nofull = secondElement.get(event);
				secondElement.put(event, nofull+1);
				fulfilledForThisTracePrecedencePart.put(lastActivity,secondElement);
			}
			//	fulfilledConstraintsPerTraceChPrecedence.putItem(caseId, fulfilledForThisTrace);
		}

		//update the counter for the current trace and the current event
		//**********************

		int numberOfEvents = 1;
		if(!seenEvents.containsKey(event)){
			seenEvents.put(event, numberOfEvents);
		}else{
			numberOfEvents = seenEvents.get(event);
			numberOfEvents++;
			seenEvents.put(event, numberOfEvents); 
		}
		lastActivity = event;



		if(isLastEvent){

			for(String param1 : fulfilledForThisTracePrecedencePart.keySet()) {
				for(String param2 : fulfilledForThisTracePrecedencePart.get(param1).keySet()) {
					if(!param1.equals(param2)){
						boolean satisfiedBool = false;
						boolean violatedBool = false;
						if(seenEvents.containsKey(param1)){
							if (fulfilledForThisTraceResponsePart.get(param1).get(param2) == seenEvents.get(param1)) {
								satisfiedBool = true;
							} else {
								violatedBool = true;
								int violated = violatedTraces.get(param1).get(param2);
								violated ++;
								HashMap<String, Integer> viol = violatedTraces.get(param1);
								if(viol==null){
								 viol = new HashMap<String, Integer>();
								}
								viol.put(param2, violated);
								violatedTraces.put(param1, viol);
							}
						}
						if(seenEvents.containsKey(param2)){
							if (fulfilledForThisTracePrecedencePart.get(param1).get(param2) == seenEvents.get(param2)) {
								if (satisfiedBool) {
									int satisfied = satisfiedTraces.get(param1).get(param2);
									satisfied ++;
									HashMap<String, Integer> sat = satisfiedTraces.get(param1);
									if(sat==null){
									 sat = new HashMap<String, Integer>();
									}
									sat.put(param2, satisfied);
									satisfiedTraces.put(param1, sat);
								}
							} else {
								if (!violatedBool) {
									int violated = violatedTraces.get(param1).get(param2);
									violated ++;
									HashMap<String, Integer> viol = violatedTraces.get(param1);
									if(viol==null){
									 viol = new HashMap<String, Integer>();
									}
									viol.put(param2, violated);
									violatedTraces.put(param1, viol);
								}
							}
						}
					}
				}
			}
		}
	}


	@Override
	public void updateModel(DeclareModel d, int completedTraces) {
		for(String param1 : fulfilledForThisTracePrecedencePart.keySet()) {
			for(String param2 : fulfilledForThisTracePrecedencePart.get(param1).keySet()) {
				if(!param1.equals(param2)){
					int vacuouslySatisfied = completedTraces - satisfiedTraces.get(param1).get(param2) - violatedTraces.get(param1).get(param2);
					//	HashMap<String, Integer> map = new HashMap<String, Integer>();
					//	map.put(param2, vacuouslySatisfied);
					//	vacuouslySatisfiedTraces.put(param1, map);
					d.addChainSuccession(param1, param2, completedTraces, satisfiedTraces.get(param1).get(param2), vacuouslySatisfied, violatedTraces.get(param1).get(param2));
				}
			}
		}
	}

}
