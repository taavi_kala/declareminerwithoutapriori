package org.processmining.plugins.declareminer.trace.constraints;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.processmining.plugins.declareminer.enumtypes.DeclareTemplate;
import org.processmining.plugins.declareminer.trace.TemplateReplayer;
import org.processmining.plugins.declareminer.util.DeclareModel;

public class Succession implements TemplateReplayer {

	HashMap<String, HashMap<String, Integer>> pendingForThisTrace = new HashMap<String, HashMap<String, Integer>>();
	private HashMap<String,Integer> pastEvents = null;
	HashMap<String, HashMap<String, Integer>> satisfiedTraces = new HashMap<String, HashMap<String, Integer>>();
	//HashMap<String, HashMap<String, Integer>> vacuouslySatisfiedTraces = new HashMap<String, HashMap<String, Integer>>();
	HashMap<String, HashMap<String, Integer>> violatedTraces = new HashMap<String, HashMap<String, Integer>>();
	//int satisfiedTraces = 0;
	//int vacuouslySatisfiedTraces = 0;
	//int violatedTraces = 0;
	HashMap<String, HashMap<String, Integer>> fulfilledForThisTrace = new HashMap<String, HashMap<String, Integer>>();


	public Succession (Map<DeclareTemplate, List<List<String>>> declareTemplateCandidateDispositionsMap){
		for(List<String> params : declareTemplateCandidateDispositionsMap.get(DeclareTemplate.Succession)){
			String param1 = params.get(0);
			String param2 = params.get(1);
			HashMap<String, Integer> pend = pendingForThisTrace.get(param1);
			if(pend==null){
				pend = new HashMap<String, Integer>();
			}
			pend.put(param2, 0);
			pendingForThisTrace.put(param1, pend);

			HashMap<String, Integer> ful = fulfilledForThisTrace.get(param1);
			if(ful==null){
				ful = new HashMap<String, Integer>();
			}
			ful.put(param2, 0);
			fulfilledForThisTrace.put(param1, ful);

			HashMap<String, Integer> sat = satisfiedTraces.get(param1);
			if(sat==null){
				sat = new HashMap<String, Integer>();
			}
			sat.put(param2, 0);
			satisfiedTraces.put(param1, sat);

			HashMap<String, Integer> viol = violatedTraces.get(param1);
			if(viol==null){
				viol = new HashMap<String, Integer>();
			}
			viol.put(param2, 0);
			violatedTraces.put(param1, viol);

		}
		pastEvents = new HashMap<String,Integer>();
	}




	@Override
	public void process(String event, boolean isANewTrace, boolean isLastEvent) {

		if(isANewTrace){
			for(String param1 : pendingForThisTrace.keySet()){
				for(String param2 : pendingForThisTrace.get(param1).keySet()){
					HashMap<String, Integer> pend = pendingForThisTrace.get(param1);
					if(pend==null){
					 pend = new HashMap<String, Integer>();
					}
					pend.put(param2, 0);
					pendingForThisTrace.put(param1, pend);
					
					
					HashMap<String, Integer> ful = fulfilledForThisTrace.get(param1);
					if(ful==null){
					 ful = new HashMap<String, Integer>();
					}
					ful.put(param2, 0);
					fulfilledForThisTrace.put(param1, ful);
				}
			}
			pastEvents = new HashMap<String,Integer>();
		}

		//if (activityLabelsSuccession.size() > 1) {
		for (String firstElement : pendingForThisTrace.keySet()) {
			if (pendingForThisTrace.get(firstElement).containsKey(event)) {
				HashMap<String, Integer> secondElement = pendingForThisTrace.get(firstElement);
				secondElement.put(event, 0);
				pendingForThisTrace.put(firstElement, secondElement);
				//	pendingConstraintsPerTrace.putItem(caseId, pendingForThisTrace);
				//					pendingConstraintsPerTrace.put(caseId, pendingForThisTrace);
			}
		}
		if(pendingForThisTrace.containsKey(event)){
			HashMap<String, Integer> secondElement = pendingForThisTrace.get(event);
			for (String second : secondElement.keySet()) {
				if (!second.equals(event)) {
					Integer pendingNo = secondElement.get(second);
					pendingNo++;
					secondElement.put(second, pendingNo);
				}
			}
			pendingForThisTrace.put(event, secondElement);
		}
		
		for (String targetEvent : fulfilledForThisTrace.keySet()) {
			if (fulfilledForThisTrace.get(targetEvent).containsKey(event)) {
				HashMap<String, Integer> secondElement = null;
				int fulfillments = 0;
				//	if (fulfilledForThisTrace.containsKey(activatingEvent)) {
				secondElement = fulfilledForThisTrace.get(targetEvent);
				//	}
				if (secondElement.containsKey(event)) {
					fulfillments = secondElement.get(event);
				}
				if (pastEvents.containsKey(targetEvent)) {
					secondElement.put(event, fulfillments + 1);
					fulfilledForThisTrace.put(targetEvent, secondElement);
				}
			}
		}
		//	pendingConstraintsPerTraceSuccession.putItem(caseId, pendingForThisTrace);
		//			pendingConstraintsPerTraceSuccession.put(caseId, pendingForThisTrace);

		// activityLabelsCounter.put(trace, counter);
		//}

		// update the counter for the current trace and the current event
		// **********************

		int numberOfEvents = 1;
		if (!pastEvents.containsKey(event)) {
			pastEvents.put(event, numberOfEvents);
		} else {
			numberOfEvents = pastEvents.get(event);
			numberOfEvents++;
			pastEvents.put(event, numberOfEvents);
		}
		//	activityLabelsCounterSuccession.putItem(caseId, counter);
		// ***********************

		if(isLastEvent){

			for(String param1 : pendingForThisTrace.keySet()) {
				for(String param2 : pendingForThisTrace.get(param1).keySet()) {
					if(!param1.equals(param2)){

						// let's generate successions


						//		for(String caseId : activityLabelsCounterSuccession.keySet()) {
						//		if (finishedTraces.containsKey(caseId) && finishedTraces.getItem(caseId) == true) {
						//	HashMap<String, Integer> counter = activityLabelsCounterSuccession.getItem(caseId);
						//	HashMap<String, HashMap<String, Integer>> pendingForThisTrace = pendingConstraintsPerTraceSuccession.getItem(caseId);
						//	if (pendingForThisTrace == null) {
						//		pendingForThisTrace = new HashMap<String, HashMap<String, Integer>>();
						//	}
						//	HashMap<String, HashMap<String, Integer>> fulfillForThisTrace = fulfilledConstraintsPerTraceSuccession.getItem(caseId);
						//	if (fulfillForThisTrace == null) {
						//		fulfillForThisTrace = new HashMap<String, HashMap<String, Integer>>();
						//	}
						boolean satisfiedBool = false;
						boolean violatedBool = false;
						//	if(pastEvents.containsKey(param1)){
						//	if(pendingForThisTrace.containsKey(param1)){
						//	if(pendingForThisTrace.get(param1).containsKey(param2)){
						if(pendingForThisTrace.get(param1).get(param2) == 0) {
							satisfiedBool = true;
						} else if (pendingForThisTrace.get(param1).get(param2) > 0) {
							int violated = violatedTraces.get(param1).get(param2);
							violated ++;
							HashMap<String, Integer> viol = violatedTraces.get(param1);
							if(viol==null){
								viol = new HashMap<String, Integer>();
							}
							viol.put(param2, violated);
							violatedTraces.put(param1, viol);
							violatedBool = true;
						}
						//	}else{
						//		violatedTraces++;
						//		violated = true;
						//	}
						//	}else{
						//		violatedTraces++;
						//		violated = true;
						//	}
						//	}

						//	if(fulfillForThisTrace.containsKey(param1)){
						//	if(fulfillForThisTrace.get(param1).containsKey(param2)){
						if(pastEvents.containsKey(param2)){
							if (fulfilledForThisTrace.get(param1).get(param2) == pastEvents.get(param2)) {
								if (satisfiedBool) {
									int satisfied = satisfiedTraces.get(param1).get(param2);
									satisfied ++;
									HashMap<String, Integer> sat = satisfiedTraces.get(param1);
									if(sat==null){
										sat = new HashMap<String, Integer>();
									}
									sat.put(param2, satisfied);
									satisfiedTraces.put(param1, sat);
								}
							} else {
								if (!violatedBool) {
									int violated = violatedTraces.get(param1).get(param2);
									violated ++;
									HashMap<String, Integer> viol = violatedTraces.get(param1);
									if(viol==null){
										viol = new HashMap<String, Integer>();
									}
									viol.put(param2, violated);
									violatedTraces.put(param1, viol);
								}
							}
						}
					}
					//			}else{
					//			if (!violated) {
					//				violatedTraces++;
					//			}
					//		}
					//	}else{
					//		if (!violated) {
					//			violatedTraces++;
					//		}
					//	}


					//}
					//			}
					//		}
				}
			}
		}
	}

	@Override
	public void updateModel(DeclareModel d, int completedTraces) {

		for(String param1 : pendingForThisTrace.keySet()) {
			for(String param2 : pendingForThisTrace.get(param1).keySet()) {
				if(!param1.equals(param2)){

					int vacuouslySatisfiedTraces = completedTraces - satisfiedTraces.get(param1).get(param2) - violatedTraces.get(param1).get(param2);

					d.addSuccession(param1, param2, completedTraces, satisfiedTraces.get(param1).get(param2), vacuouslySatisfiedTraces, violatedTraces.get(param1).get(param2));
					// d.addNotSuccession(param1, param2, completedTraces,violatedTraces , vacuouslySatisfiedTraces,satisfiedTraces );
				}
			}
		}
	}


}
