require 'fileutils'

class TestsRunner
  attr_reader :conf_path, :logs_path

  ALPHAS = [
    '100',
    '0'
  ]

  MIN_SUPPORTS = [
    '60',
    '70',
    '80',
    '90',
    '100'
  ]

  TEMPLATES = [
    'Succession',
    'Alternate_Succession',
    'Chain_Succession',
    'Choice',
    'Exclusive_Choice',
    'Existence',
    'Existence2',
    'Existence3',
    'Init',
    'Absence',
    'Absence2',
    'Absence3',
    'Exactly1',
    'Exactly2',
    'Precedence',
    'Alternate_Precedence',
    'Chain_Precedence',
    'Responded_Existence',
    'Response',
    'Alternate_Response',
    'Chain_Response',
    'CoExistence',
  ]

  PROJECTS = [
    'declareminerextract',
    'declareminerextractold'
  ]

  def run!
    puts 'Starting with tests'
    run_logs('synthetic-logs')
    run_logs('bpi-challenges')
    puts "Done! Results can be seen from #{shared_log.path}"
  end

private
  
  def run_logs(dir)
    @logs_path = dir
    @conf_path = "#{File.dirname(__FILE__)}/conf.properties"
    puts "Running logs: #{logs_path}"
    logs = load_logs
    PROJECTS.each do |project|
      logs.each do |log|
        confs = prepare_confs_for(log)
        confs.each do |conf|
          write_conf(conf)
          3.times do |i|
            puts "Run nr #{i}"
            run_command(project)
          end
        end
      end
    end
    puts "Finished running logs: #{logs_path}"
  end

  def load_logs
    Dir.entries(logs_path).reject{|e| ['.', '..'].include?(e)}
  end

  def prepare_confs_for(log)
    confs = []
    perspectives = 'Control_Flow'
    map_template_configuration = 'DiscoverProvidedTemplatesAcrossAllActivitesInLog'
    criterias = (logs_path == 'synthetic-logs' ? 'AllActivitiesIgnoringEventTypes' : 'AllActivitiesWithEventTypes')
    unified_log_path = shared_log.path
    log_file_path = "#{File.dirname(__FILE__)}/#{logs_path}/#{log}"
    ALPHAS.each do |alpha|
      MIN_SUPPORTS.each do |min_support|
        TEMPLATES.each do |template|
          output_path = "#{File.dirname(__FILE__)}/output_xmls/#{template}_alpha_#{alpha}_minsupport_#{min_support}_#{File.basename(log, '.*')}"
          confs << begin
"log_file_path=#{log_file_path}
templates=#{template}
perspectives=#{perspectives}
map_template_configuration=#{map_template_configuration}
min_support=#{min_support}
alpha=#{alpha}
criterias=#{criterias}
output_path=#{output_path}.xml
output_file_type=XML
miner_type=DeclareMiner
unified_log_path=#{unified_log_path}"
          end
        end

        output_path = "#{File.dirname(__FILE__)}/output_xmls/all_templates_alpha_#{alpha}_minsupport_#{min_support}_#{File.basename(log, '.*')}"

        confs << begin
"log_file_path=#{log_file_path}
templates=#{TEMPLATES.join(',')}
perspectives=#{perspectives}
map_template_configuration=#{map_template_configuration}
min_support=#{min_support}
alpha=#{alpha}
criterias=#{criterias}
output_path=#{output_path}.xml
output_file_type=XML
miner_type=DeclareMiner
unified_log_path=#{unified_log_path}"
        end
      end
    end

    confs
  end

  def write_conf(conf)
    puts "\n\n\n"
    puts conf
    File.open(conf_path, 'wb') {|c| c.write(conf)}
  end

  def run_command(project)
    project_path = "#{File.dirname(__FILE__)}/#{project}"
    command = "java -Xmx16g -XX:+UseConcMarkSweepGC -cp #{project_path}/declareminerextract_v5.jar BetterMiner #{conf_path}"
    puts command
    system(command)
  end

  def shared_log
    @shared_log ||= begin
      log_path = "#{File.dirname(__FILE__)}/all_results.log"
      FileUtils.touch log_path
      File.new(log_path)
    end
  end
end

TestsRunner.new().run!
